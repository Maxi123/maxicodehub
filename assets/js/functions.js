    const itemsMenu = document.querySelectorAll(".nav-item");
    const navbarCollapse = document.getElementById('navbarSupportedContent');
    const navbarCollapse2 = document.getElementById('navbarSupportedContent2');
    const headerScroll = document.getElementById('header-scroll');

    
    itemsMenu.forEach(item => {
        item.addEventListener("click", function () {
            navbarCollapse.classList.remove('show');
            navbarCollapse2.classList.remove('show');
        });
    });

    window.addEventListener('scroll', function(){

        if(this.window.scrollY > 100){
            headerScroll.classList.remove('d-none');
        }else{
            headerScroll.classList.add('d-none');
        };

    });